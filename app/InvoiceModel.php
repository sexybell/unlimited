<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceModel extends Model
{
    protected $table = 'invoice';
    public $timestamps = true;

    public function invoice()
    {
        return $this->hasMany('App\InvoiceDetailModel', 'invoice_id', 'id');
    }
}
