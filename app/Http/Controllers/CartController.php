<?php

namespace App\Http\Controllers;

use App\InvoiceDetailModel;
use App\InvoiceModel;
use App\ProductModel;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CartController extends Controller
{
    public function addCart($id)
    {
        $product = ProductModel::find($id);
        if ($product->promotion_price > 0) {
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1,
                'price' => $product->promotion_price,
                'options' => ['image' => $product->image,
                    'promotion_price' => $product->promotion_price]]);
        } else {
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1,
                'price' => $product->price,
                'options' => ['image' => $product->image
                ]]);
        }
        // dd(Cart::content());
        return redirect()->back()->with('notification', 'Added');
    }

    public function addSecond($id, Request $request)
    {
        $product = ProductModel::find($id);
        if ($product->promotion_price > 0) {
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->qty,
                'price' => $product->promotion_price,
                'options' => ['image' => $product->image,
                    'promotion_price' => $product->promotion_price]]);
        } else {
            Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->qty,
                'price' => $product->price,
                'options' => ['image' => $product->image
                ]]);
        }
        // dd(Cart::content());
        return redirect()->back()->with('notification', 'Added');
    }

    public function showCart()
    {
        $data['items'] = Cart::content();
        $data['total'] = Cart::total();
        return view('clients\checkout', $data);
    }

    public function delete($id)
    {
        Cart::remove($id);
        return redirect()->back()->with('notification','Delete sussessfully');
    }

    public function insertDetail(Request $request)
    {
        if (Auth::user() && Auth::user()->role === 1) {
            Cart::destroy();
            return redirect('/')->with('notification', 'Admininstrator cant buy anything');
        } else {
            $this->validate($request,
                [
                    'name' => 'required',
                    'email' => 'required', 'email',
                    'phone_number' => 'required', 'numeric', 'max:12',
                    'address' => 'required'
                ],
                [
                    'name.required' => 'Please enter your name',
                    'email.email' => 'The email address you supplied is invalid.',
                    'email.required' => 'Please enter your email',
                    'phone_number.required' => 'Please enter your phone number',
                    'phone_number.numeric' => 'Please enter number only',
                    'phone_number.max' => 'Your phone number is too long.',
                    'address.required' => 'Please enter your address'
                ]
            );
            $invoice = new InvoiceModel;
            $total = Cart::total();
            $invoice->invoice_email = $request->email;
            $invoice->total = $total;
            $invoice->name = $request->name;
            $invoice->address = $request->address;
            $invoice->phone_number = $request->phone_number;
            $invoice->created_at = now();
            $invoice->save();
            if ($request) {
                foreach (Cart::content() as $item) {
                    $detail = new InvoiceDetailModel;
                    $detail->product_id = $item->id;
                    $detail->price = $item->price;
                    $detail->quantity = $item->qty;
                    $detail->invoice_id = $invoice->id;
                    $detail->created_at = now();
                    $detail->save();
                }
            }
            $mail = $request->email;
            $content = Cart::content();
            $total = Cart::total();
            $data = array('email' => $mail, 'content' => $content, 'total' => $total);
            Mail::send('clients/email', $data, function ($message) use ($data) {
                $message->from('nguyenhaiduong251298@gmail.com', 'Unlimited Shop');
                $message->to($data['email']);
                $message->subject('Your Order');
            });
            $bigdata['checkout'] = $request;
            $bigdata['total'] = Cart::total();
            return view('clients\alert', $bigdata);

        }
    }

    public function destroy()
    {
        Cart::destroy();
        return redirect('/');
    }
}
