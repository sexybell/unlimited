<?php

namespace App\Http\Controllers;

use App\InvoiceDetailModel;
use App\InvoiceModel;
use App\ProductModel;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index()
    {
        $invoice = InvoiceModel::paginate(10);
        return view('admin\invoices\list', ['invoice' => $invoice]);
    }

    public function detail($id)
    {
        $detail = InvoiceDetailModel::where('invoice_id', '=', $id)->paginate(10);
        return view('admin\invoices\detail', ['detail' => $detail]);
    }

    public function searchInvoice()
    {
        return view('clients\searchinvoice');
    }

    public function searchdetail(Request $request)
    {
        $email = $request->email;
        $invoice = InvoiceModel::where('invoice_email', 'like', '%' . $email . '%')->paginate(6);
        return view('clients/searchresult', compact('email', 'invoice'));
    }

    public function invoicedetail($id)
    {
        $detail = InvoiceDetailModel::where('invoice_id', '=', $id)->paginate(6);

        return view('clients\invoicedetail', ['detail' => $detail]);
    }
}
