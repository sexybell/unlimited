<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetailModel extends Model
{
    protected $table = 'invoice_detail';
    public $timestamps = true;


    public function detailProduct()
    {
        return $this->belongsTo('App\ProductModel', 'product_id', 'id');
    }

}
