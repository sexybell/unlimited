<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table = 'products';
    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo('App\CategoryModel', 'id_category', 'id');
    }

    public function invoice_detail()
    {
        return $this->hasMany('App\InvoiceDetailModel', 'product_id', 'id');
    }
}
