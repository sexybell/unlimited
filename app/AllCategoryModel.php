<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllCategoryModel extends Model
{
    protected $table = 'allcategories';
    public $timestamps = true;

    public function allcategory()
    {
        return $this->hasMany('App\CategoryModel', 'category_id', 'id');
    }

}
