@extends('layouts\app')
@section('content')
    <div class="container">

        <div class="row" style="width:100%">
            <div class="col-md-6" style="margin:auto">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Create Product</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    @if(session('notification'))
                        <div class="alert alert-success">
                            {{ session('notification') }}
                        </div>
                    @endif
                    <form action="{{route('products.add')}}" method="POST" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="card-body">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" id="exampleInputEmail1" name="name">
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                <label for="exampleInputPassword1">Description</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="description">
                                <span class="text-danger">{{ $errors->first('description') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }} ">
                                <label for="exampleInputPassword1">Price</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="price">
                                <span class="text-danger">{{ $errors->first('price') }}</span>
                            </div>
                            <div class="form-group {{ $errors->has('promotion_price') ? ' has-error' : '' }}">
                                <label for="exampleInputPassword1">Promotion Price</label>
                                <input type="text" class="form-control" id="exampleInputPassword1"
                                       name="promotion_price">
                                <span class="text-danger">{{ $errors->first('promotion_price') }}</span>
                            </div>
                            <div class="form-group ">
                                <label for="my-select">Category</label>
                                <select id="my-select" class="form-control" name="category_id">
                                    <option value="0">Select Category</option>
                                    @foreach($all as $i)
                                        <option value="{{$i->id}}">{{$i->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="my-select">Brand</label>
                                <select id="my-select" class="form-control" name="id_category">
                                    <option value="0">Select Sub Category</option>
                                </select>
                            </div>
                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="exampleInputFile">Image</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                                <span class="text-danger">{{ $errors->first('image') }}</span>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function ($) {
            $('select[name=category_id]').on('change', function () {
                var selected = $(this).find(":selected").attr('value');
                var base_url = 'http://127.0.0.1:8000/admin/products';
                $.ajax({
                    url: base_url + '/category/' + selected + '/subcategory/',
                    type: 'GET',
                    dataType: 'json',

                }).done(function (data) {
                    var select = $('select[name=id_category]');
                    select.empty();
                    select.append('<option value="0">Select Sub Category</option>');
                    $.each(data, function (key, value) {
                        select.append('<option value=' + value.id + '>' + value.name + '</option>');
                    });

                })
            });
        });

    </script>

@endsection
