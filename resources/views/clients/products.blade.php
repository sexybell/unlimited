@extends('welcome')
@section('content')

    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- Product main img -->

                <form action="/cart/addSecond/{{$product->id}}">
                    <div class="col-md-7">

                        <div id="product-main-img">
                            <img src="/upload/products/{{$product->image}}" alt="">
                        </div>
                    </div>
                    <div class="col-md-5">

                        <div class="product-details">


                            <h2 class="product-name"> {{ $product->name }} </h2>
                            <div>
                                <h3 class="product-price">@if($product->promotion_price > 0)
                                        <del class="product-old-price">{{number_format($product->price,'0')}}đ</del>
                                        {{number_format($product->promotion_price,'0')}}
                                        đ@else {{number_format($product->price,'0')}}đ@endif</h3>
                                <span class="product-available">In Stock</span>
                            </div>
                            <p>{{$product->description}}</p>

                            <div class="add-to-cart">
                                <div class="qty-label">
                                    Qty
                                    <div class="input-number">
                                        <input type="number" name='qty' value="1" max="10">
                                    </div>
                                </div>
                                <button class="add-to-cart-btn" type="submit"><i class="fa fa-shopping-cart"></i> add to
                                    cart
                                </button>
                            </div>

                            <ul class="product-btns">


                                <ul class="product-links">
                                    <li>Category:</li>
                                    <li><a href="#">Headphones</a></li>
                                    <li><a href="#">Accessories</a></li>
                                </ul>

                        </div>

                    </div>
                </form>

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /SECTION -->

    <!-- Section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h3 class="title">Related Products</h3>
                    </div>
                </div>

                <!-- product -->

                @foreach($sameProduct as $item)
                    <div class="col-md-3 col-xs-6">
                        <div class="product">
                            <div class="product-img">
                                <img src="/upload/products/{{$item->image}}" alt="">
                                @if($item->promotion_price > 0)
                                    <div class="product-label">
                                <span
                                    class="sale">{{number_format(100- $item->promotion_price/$item->price*100)}}%</span>
                                    </div>
                                @endif
                            </div>
                            <div class="product-body">
                                <p class="product-category">Category</p>
                                <h3 class="product-name"><a href="/cart/products/{{ $item->id }}">{{$item->name}}</a>
                                </h3>
                                <h4 class="product-price">@if($item->promotion_price > 0)
                                        <del
                                            class="product-old-price">{{number_format($item->price,'0')}}
                                            đ
                                        </del>
                                        {{number_format($item->promotion_price,'0')}}
                                        đ@else {{number_format($item->price,'0')}}đ@endif</h4>

                            </div>
                            <div class="add-to-cart">
                                <a href="/cart/addCart/{{$item->id}}">
                                    <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i>
                                        add to cart
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
            @endforeach

            <!-- /product -->

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /Section -->


@endsection
