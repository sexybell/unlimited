@extends('welcome')
@section('content')
    <div class="row container" style="margin:auto">
        <p>Từ khoá: {{ $search }}</p>
        @foreach ($products as $item)
            @if($item->is_active ===1)
                <div class="col-md-3">


                    <div class="product">
                        <div class="product-img">
                            <img src="/upload/products/{{$item->image}}" alt="">
                            <div class="product-label">
                                <span class="new">NEW</span>
                            </div>
                        </div>
                        <div class="product-body">
                            <p class="product-category">{{$item->category->name}}</p>
                            <h3 class="product-name"><a href="#">{{$item->name}}</a></h3>
                            <h4 class="product-price">@if($item->promotion_price > 0)
                                    <del class="product-old-price">{{number_format($item->price,'0')}}đ</del>
                                    {{number_format($item->promotion_price,'0')}}
                                    đ@else {{number_format($item->price,'0')}}
                                    đ@endif</h4>
                        </div>
                        <div class="add-to-cart">
                            <a href="/cart/addCart/{{$item->id}}">
                                <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                            </a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
        {!! $products->links() !!}
    </div>
@endsection
