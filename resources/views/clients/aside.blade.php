@extends('clients\store')
@section('store')

    <!-- store top filter -->
    <div class="store-filter clearfix">
        <div class="store-sort">
            <label>
                Sort By:
                <select class="input-select">
                    <option value="0">Popular</option>
                    <option value="1">Position</option>
                </select>
            </label>

            <label>
                Show:
                <select class="input-select">
                    <option value="0">20</option>
                    <option value="1">50</option>
                </select>
            </label>
        </div>
        <ul class="store-grid">
            <li class="active"><i class="fa fa-th"></i></li>
            <li><a href="#"><i class="fa fa-th-list"></i></a></li>
        </ul>
    </div>
    <!-- /store top filter -->

    <!-- store products -->
    <div class="row">
        <div class="clearfix visible-sm visible-xs"></div>
        <!-- product -->
        @foreach ($product as $item)
            @if($item->is_active === 1)
                <div class="col-md-3">

                    <div class="product">
                        <div class="product-img">
                            <img src="/upload/products/{{$item->image}}" alt="">
                            @if($item->promotion_price > 0)
                                <div class="product-label">
                                <span
                                    class="sale">{{number_format(100- $item->promotion_price/$item->price*100)}}%</span>
                                </div>
                            @endif
                        </div>
                        <div class="product-body">
                            <p class="product-category">{{$item->category->name}}</p>
                            <h3 class="product-name"><a href="/cart/products/{{ $item->id }}">{{$item->name}}</a></h3>
                            <h4 class="product-price">@if($item->promotion_price > 0)
                                    <del class="product-old-price">{{number_format($item->price,'0')}}đ</del>
                                    {{number_format($item->promotion_price,'0')}}
                                    đ@else {{number_format($item->price,'0')}}
                                    đ@endif</h4>
                        </div>
                        <div class="add-to-cart">
                            <a href="/cart/addCart/{{$item->id}}">
                                <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                            </a>
                        </div>
                    </div>

                </div>
        @endif
    @endforeach
    <!-- /product -->

    </div>
    <!-- /store products -->

    <!-- store bottom filter -->
    <div class="store-filter clearfix">
        <span class="store-qty">Showing 20-100 products</span>

        {!! $product->links() !!}

    </div>
    <!-- /store bottom filter -->

@endsection
