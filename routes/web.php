<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'backmiddleware'], function () {
    Route::get('/', 'ClientController@index');
    Route::get('aside/{id}', 'ClientController@aside');
    Route::get('/search', 'ClientController@search');
    Route::get('/store/{id}', 'ClientController@store');
    Route::get('/searchInvoice', 'InvoiceController@searchInvoice');
    Route::get('/searchdetail', 'InvoiceController@searchdetail');
    Route::get('/invoicedetail/{id}', 'InvoiceController@invoicedetail');
    Route::post('/mailsending', 'ClientController@mailsending');
    Route::group(['prefix' => 'cart'], function () {
        Route::get('/addCart/{id}', 'CartController@addCart');
        Route::get('/addSecond/{id}', 'CartController@addSecond');
        Route::get('/showCart', 'CartController@showCart');
        Route::post('/insertDetail', 'CartController@insertDetail');
        Route::get('/products/{id}', 'ClientController@products');
        Route::get('/destroy', 'CartController@destroy');
        Route::get('/delete/{id}', 'CartController@delete');
    });


    Auth::routes();


    Route::group(['prefix' => 'admin', 'middleware' => 'isAdmin'], function () {
        Route::get('/', function () {
            return view('home');
        });
        Route::group(['prefix' => 'categories'], function () {
            Route::get('/allcate', 'CategoryController@all');
            Route::get('/list', 'CategoryController@index');
            Route::get('/addForm', 'CategoryController@addForm');
            Route::post('/add', 'CategoryController@add');
            Route::get('/editForm/{id}', 'CategoryController@editForm');
            Route::post('/edit/{id}', 'CategoryController@edit');
            Route::get('/delete/{id}', 'CategoryController@delete');
            Route::get('/filter/{id}', 'CategoryController@filterProduct');
            Route::get('/filterCate/{id}', 'CategoryController@filterCate');
        });
        Route::group(['prefix' => 'products'], function () {
            Route::get('list', 'ProductController@index');
            Route::get('addForm', 'ProductController@addForm');
            Route::post('add', 'ProductController@add')->name('products.add');
            Route::get('editForm/{id}', 'ProductController@editForm');
            Route::post('edit/{id}', 'ProductController@edit')->name('products.edit');
            Route::get('hide/{id}', 'ProductController@hide');
            Route::get('showup/{id}', 'ProductController@showup');
            Route::get('/category/{id}/subcategory', 'ProductController@subcategory');
        });
        Route::get('invoice', 'InvoiceController@index');
        Route::get('detail/{id}', 'InvoiceController@detail');


    });
});
